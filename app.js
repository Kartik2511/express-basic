var express = require('express');
var bodyParser = require('body-parser');

var app = express();
var urlEncodedParser = bodyParser.urlencoded({extended: false});

app.listen(3000);
app.set('view engine', 'ejs');

app.use('/css', express.static('views/css'));
app.use('/mycss', express.static('views/mycss'));
app.use('/mycssjs', express.static('views/mycssjs'));
app.use('/fonts', express.static('views/fonts'));
app.use('/img', express.static('views/img'));
app.use('/js', express.static('views/js'));

app.use(bodyParser.json());

app.get('/Home', function(req, res){
	res.send('<h1>You are on Home page</h1>');
	});

app.get('/AddCrop', function(req, res){
	res.render('HTML');
	});

app.post('/AddCrop',urlEncodedParser, function(req, res){
	console.log(req.body);
	res.send(req.body);
	});


app.get('/Contact', function(req, res){
	res.send(req.query);
	});

app.get('/Profile/:id', function(req, res){
	console.log(req.params.id);
	res.send(req.params.id + " -- from /:id");
	});
